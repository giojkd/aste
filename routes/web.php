<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', '\App\Http\Controllers\FrontController@home')->name('home');
Route::get('/product/{name}/{id}', '\App\Http\Controllers\FrontController@product')->name('product');
Route::get('/register', '\App\Http\Controllers\FrontController@register')->name('register');
Route::post('/register-submit', '\App\Http\Controllers\FrontController@registerSubmit')->name('registerSubmit');
Route::post('/register-submit-update', '\App\Http\Controllers\FrontController@registerSubmitUpdate')->name('registerSubmitUpdate');
Route::get('/user', '\App\Http\Controllers\FrontController@user')->name('user');
Route::get('/login', '\App\Http\Controllers\FrontController@login')->name('login');
Route::post('/login-submit', '\App\Http\Controllers\FrontController@loginSubmit')->name('loginSubmit');
Route::get('/logout', '\App\Http\Controllers\FrontController@logout')->name('logout');

/** CATCH-ALL ROUTE for Backpack/PageManager - needs to be at the end of your routes.php file  **/
Route::get('{page}/{subs?}', ['uses' => '\App\Http\Controllers\FrontController@page'])
    ->where(['page' => '^(((?=(?!admin))(?=(?!\/)).))*$', 'subs' => '.*']);

