@extends('main')
@section('content')


<div class="container py-4 py-lg-5 my-4">
      <div class="row justify-content-center">
        <div class="col-md-6 ">
          <div class="card border-0 shadow">
            <div class="card-body">
              <h2 class="h4 mb-4">Hai già un account? Accedi</h2>


              <form class="needs-validation" novalidate="" method="post" action="{{ Route('loginSubmit') }}">
                  @csrf
                <div class="input-group mb-3"><i class="ci-mail position-absolute top-50 translate-middle-y text-muted fs-base ms-3"></i>
                  <input class="form-control rounded-start" type="email" placeholder="Email" required="" name="email" >

                </div>
                <div class="input-group mb-3"><i class="ci-locked position-absolute top-50 translate-middle-y text-muted fs-base ms-3"></i>
                  <div class="password-toggle w-100">
                    <input class="form-control" type="password" placeholder="Password" required="" name="password">
                  </div>
                </div>
                @if($errors->has('email'))
                        <div class="invalid-feedback d-block mt-0 mb-3" style="margin-top: -10px!important">{{ $errors->first('email') }}</div>
                    @endif
                {{--
                <div class="d-flex flex-wrap justify-content-between">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" checked="" id="remember_me">
                    <label class="form-check-label" for="remember_me">Remember me</label>
                  </div><a class="nav-link-inline fs-sm" href="account-password-recovery.html">Forgot password?</a>
                </div>
                 --}}

                <div class="text-end">
                  <button class="btn btn-blue" type="submit"><i class="ci-sign-in me-2 ms-n21"></i>Accedi</button>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>


@endsection
