@extends('main')
@section('content')

    <div class="container">
        <div class="row mt-4">
            <div class="col-md-6">
                <h2 class="mb-4">{{ Auth::user()->name }}</h2>
            </div>
            <div class="col-md-6 text-end">
                <a href="{{ Route('logout') }}" class="btn btn-danger">Esci</a>
            </div>
        </div>
        <div class="row my-4">

            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-data-tab" data-bs-toggle="tab" data-bs-target="#nav-data"
                            type="button" role="tab" aria-controls="nav-data" aria-selected="true">I miei dati</button>
                        <button class="nav-link" id="nav-bids-tab" data-bs-toggle="tab" data-bs-target="#nav-bids"
                            type="button" role="tab" aria-controls="nav-bids" aria-selected="false">Le mie aste</button>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">

                    <!-- USER DATA TABS -->

                    <div class="tab-pane fade show active" id="nav-data" role="tabpanel" aria-labelledby="nav-data-tab">
                        <h3 class="my-4 py-4">Modifica Anagrafica</h3>
                        <form action="{{ Route('registerSubmitUpdate') }}" method="POST" class="needs-validation" novalidate>
                            @csrf
                            <div class="container-fluid">

                            <div class="row border-bottom mb-4">
                                <div class="col-md-3">Dati anagrafici</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Nome</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby="" name="name"
                                                required value="{{ Auth::user()->data->name }}">
                                            <div class="invalid-feedback">
                                                Il nome è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Cognome</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby="" name="surname"
                                                required value="{{ Auth::user()->data->surname }}">
                                            <div class="invalid-feedback">
                                                Il cognome è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Codice fiscale</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby=""
                                                name="fiscal_code" required value="{{ Auth::user()->data->fiscal_code }}">
                                            <div class="invalid-feedback">
                                                Il codice fiscale è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-4"></div>
                                        <div class="col-8">
                                            <p class="text-muted"><small><i class="fa fa-info-circle"></i> Inserire il
                                                    codice fiscale estero o altro identificativo se il soggetto risiede
                                                    all'estero.</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">N° carta identità/ passaporto</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby=""
                                                name="document_id" required value="{{ Auth::user()->data->document_id }}">
                                            <div class="invalid-feedback">
                                                Il numero di un documento è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Data di nascita</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="date" id="" class="form-control" aria-describedby=""
                                                name="date_of_birth" required value="{{ Auth::user()->data->date_of_birth }}">
                                            <div class="invalid-feedback">
                                                La data di nascita è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Paese di cittadinanza</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="citizenship_country" required>
                                                <option >Scegli...</option>
                                                @foreach ($countries as $country)
                                                    <option @if ($country->country_name == 'Italy') selected @endif value=""
                                                        @if(Auth::user()->data->citizenship_country == $country->country_name) selected @endif>
                                                        {{ $country->country_name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                Il paese di cittadinanza è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Nazione nascita</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="country_of_birth" required>
                                                <option >Scegli...</option>
                                                @foreach ($countries as $country)
                                                    <option @if(Auth::user()->data->country_of_birth == $country->country_name) selected @endif>
                                                        {{ $country->country_name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La nazione di nascita è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Regione nascita</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="region_of_birth" required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->unique('region')->sortBy('region') as $region)
                                                    <option @if(Auth::user()->data->region_of_birth == $region->region) selected @endif>{{ $region->region }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La regione di nascita è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Provincia nascita</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="province_of_birth" required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->unique('province')->sortBy('region') as $province)
                                                    <option @if(Auth::user()->data->province_of_birth == $province->province) selected @endif>{{ $province->province }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La provincia di nasciat è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Comune nascita</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="comun_of_birth" required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->sortBy('name') as $comune)
                                                    <option @if(Auth::user()->data->comun_of_birth == $comune->name) selected @endif>{{ $comune->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                Il comune di nascita è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">CAP nascita</label>
                                        </div>
                                        <div class="col-8">
                                            <div class="col-8">
                                                <select id="" class="form-control select2" name="zip_code_of_birth"
                                                    required>
                                                    <option >Scegli...</option>
                                                    @foreach ($comuns->sortBy('postal_code') as $comune)
                                                        <option @if(Auth::user()->data->zip_code_of_birth == $comune->postal_code) selected @endif>{{ $comune->postal_code }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">
                                                    Il CAP di nascita è obbligatoria
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row border-bottom mb-4">
                                <div class="col-md-3">Contatti</div>

                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Email Pec</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby="" name="pec"
                                                required
                                                value="{{ Auth::user()->data->pec }}">
                                            <div class="invalid-feedback">
                                                L'indirizzo Email/PEC è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Telefono</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby="" name="phone"
                                                required
                                                value="{{ Auth::user()->data->phone }}">
                                            <div class="invalid-feedback">
                                                Il telefono è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Cellulare</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby=""
                                                name="mobile_phone" required
                                                value="{{ Auth::user()->data->mobile_phone }}">
                                            <div class="invalid-feedback">
                                                Il cellulare è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom mb-4">
                                <div class="col-md-3">Residenza</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Nazione</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="residence_country" required>
                                                <option >Scegli...</option>
                                                @foreach ($countries as $country)
                                                    <option @if(Auth::user()->data->residence_country == $country->country_name) selected @endif>
                                                        {{ $country->country_name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La nazione di residenza è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Regione</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="residence_region" required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->unique('region')->sortBy('region') as $region)
                                                    <option @if(Auth::user()->data->residence_region == $region->region) selected @endif>{{ $region->region }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La regione di residenza è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Provincia</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="residence_province" required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->unique('province')->sortBy('region') as $province)
                                                    <option @if(Auth::user()->data->residence_province == $province->province) selected @endif>{{ $province->province }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La provincia di residenza è obbligatoria
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Comune</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="residence_comun" required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->sortBy('name') as $comune)
                                                    <option @if(Auth::user()->data->residence_comun == $comune->name) selected @endif>{{ $comune->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                Il comune di residenza è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Cap/Zip</label>
                                        </div>
                                        <div class="col-8">
                                            <select id="" class="form-control select2" name="residence_postal_code"
                                                required>
                                                <option >Scegli...</option>
                                                @foreach ($comuns->sortBy('postal_code') as $comune)
                                                    <option @if(Auth::user()->data->residence_postal_code == $comune->postal_code) selected @endif>{{ $comune->postal_code }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                La CAP di residenza è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Indirizzo</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby=""
                                                name="residence_address" required value="{{ Auth::user()->data->residence_address }}">
                                            <div class="invalid-feedback">
                                                L'indirizzo di residenza è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-4">
                                            <label for="" class="col-form-label">Civico</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" id="" class="form-control" aria-describedby=""
                                                name="residence_street_number" required value="{{ Auth::user()->data->residence_street_number }}">
                                            <div class="invalid-feedback">
                                                Il numero civido di residenza è obbligatorio
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom mb-4">
                                <div class="col-md-3">Domicilio</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <label for="" class="col-form-label">Il domicilio è diverso dalla
                                                residenza</label>
                                        </div>
                                        <div class="col-6">
                                            <label for=""><input
                                                {{ (Auth::user()->data->domicile_different_from_residence == 1) ? 'selected' : '' }}
                                                type="radio" name="domicile_different_from_residence"
                                                    value="1" id=""> Si</label>
                                            <label for=""><input
                                                {{ (Auth::user()->data->domicile_different_from_residence == 0) ? 'selected' : '' }}
                                                type="radio" name="domicile_different_from_residence"
                                                    value="0" id="" checked> No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="domicileDifferentFromResidence" class="d-none">
                                <div class="row border-bottom mb-4">
                                    <div class="col-md-3">Domicilio</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Nazione</label>
                                            </div>
                                            <div class="col-8">
                                                <select id="" class="form-control select2" name="domicile_country">
                                                    <option >Scegli...</option>
                                                    @foreach ($countries as $country)
                                                        <option @if(Auth::user()->data->domicile_country == $country->country_name) selected @endif
                                                            >
                                                            {{ $country->country_name }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Regione</label>
                                            </div>
                                            <div class="col-8">
                                                <select id="" class="form-control select2" name="domicile_region">
                                                    <option >Scegli...</option>
                                                    @foreach ($comuns->unique('region')->sortBy('region') as $region)
                                                        <option
                                                        @if(Auth::user()->data->domicile_region == $region->region) selected @endif
                                                        >{{ $region->region }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Provincia</label>
                                            </div>
                                            <div class="col-8">
                                                <select id="" class="form-control select2" name="domicile_province">
                                                    <option >Scegli...</option>
                                                    @foreach ($comuns->unique('province')->sortBy('region') as $province)
                                                        <option
                                                        @if(Auth::user()->data->domicile_province == $province->province) selected @endif
                                                        >{{ $province->province }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Comune</label>
                                            </div>
                                            <div class="col-8">
                                                <select id="" class="form-control select2" name="domicile_comun">
                                                    <option >Scegli...</option>
                                                    @foreach ($comuns->sortBy('name') as $comune)
                                                        <option
                                                        @if(Auth::user()->data->domicile_comun == $comune->name) selected @endif
                                                        >{{ $comune->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Cap/Zip</label>
                                            </div>
                                            <div class="col-8">
                                                <select id="" class="form-control select2" name="domicile_zip_code">
                                                    <option >Scegli...</option>
                                                    @foreach ($comuns->sortBy('postal_code') as $comune)
                                                        <option
                                                        @if(Auth::user()->data->domicile_zip_code == $comune->postal_code) selected @endif
                                                        >{{ $comune->postal_code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Indirizzo</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" id="" class="form-control" aria-describedby=""
                                                value="{{ Auth::user()->data->domicile_address }}"
                                                name="domicile_address">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row align-items-center">
                                            <div class="col-4">
                                                <label for="" class="col-form-label">Civico</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" id="" class="form-control" aria-describedby=""
                                                value="{{ Auth::user()->data->domicile_street_number }}"
                                                    name="domicile_street_number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-secondary w-100">Aggiorna</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>


                    <!-- BIDS TAB -->

                    <div class="tab-pane fade" id="nav-bids" role="tabpanel" aria-labelledby="nav-bids-tab">
                        <div class="alert alert-secondary my-4 py-4">
                            Non hai partecipato a nessuna asta ancora.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<style>
    #terms-and-conditions-container{
        height: 220px;
        overflow: scroll;
    }
</style>

<script>
    $(()=>{
        $('.select2').select2({
            theme:'bootstrap'
        });
        $('input[name="domicile_different_from_residence"]').change(function(){
            if($(this).val() == 1){
                $('#domicileDifferentFromResidence').removeClass('d-none');
            }else{
                $('#domicileDifferentFromResidence').addClass('d-none');
            }
        })

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }else{
                    form.submit();
                }

                form.classList.add('was-validated')
            }, false)
        })

    })


</script>

@endsection
