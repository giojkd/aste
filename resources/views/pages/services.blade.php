@extends('main')
@section('content')

    <div class="container">
        <div class="col-md-12">
            <h1>
                {{ $page->name }}
            </h1>
            <div>
                {!! $page->content !!}
            </div>
        </div>
    </div>

@endsection
