@extends('main')
@section('content')

    <div class="container">
        <div class="col-md-12">
            <h1>
                {{ $page->name }}
            </h1>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h2>{{ $page->company_name }}</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            {{ $page->address }} <br>
                            Tel: {{ $page->tel }} <br>
                            Fax: {{ $page->fax }}
                        </div>
                        <div class="col-md-6">
                            <b>Per informazioni o problemi inerenti alle singole aste occorre contattare esclusivamente il singolo ente gestore della gara</b> <br><br>
                            Mail: <a href="mailto:{{ $page->mail }}">{{ $page->mail }}</a> <br>
                            PEC: <a href="mailto:{{ $page->pec }}">{{ $page->pec }}</a> <br>
                            Social network: <a href="{{ $page->facebook_link }}"><i class="fa fa-facebook"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <iframe src="https://www.google.com/maps/embed/v1/place/{{ env('GOOGLE_MAPS_API_KEY') }}?q={{ $page->address }}" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>

@endsection
