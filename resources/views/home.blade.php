@extends('main')
@section('content')

    <div class="hero d-flex justify-content-center align-items-center w-100">
        <div class="hero-content d-flex w-100" >
            <div class="d-block w-100">
                <div class="container">
                    <div class="col-md-10 offset-md-1">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#" class="nav-link active" id="type-1-tab" data-bs-toggle="tab" data-bs-target="#type-1" role="tab" aria-controls="type-1" aria-selected="true"><i class="fa fa-home"></i> Immobili</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="type-2-tab" data-bs-toggle="tab" data-bs-target="#type-2" role="tab" aria-controls="type-2" aria-selected="true"><i class="fa fa-car"></i> Mobili</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <!-- TAB 1 -->
                            <div class="tab-pane fade show active bg-blue p-3" id="type-1" role="tabpanel" aria-labelledby="home-tab">
                                <form action="">
                                    <div class="row mb-2">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Categoria</label>
                                                <select name="" id="" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Tipologia</label>
                                                <select name="" id="" class="form-control"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Ricerca libera</label>
                                                <input type="text" class="form-control" placeholder="Cerca nella descrizione del bene">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Indirizzo o città</label>
                                                <input type="text" class="form-control" placeholder="Cerca su Google Maps">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Tribunale</label>
                                                <select name="" id="" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Procedura</label>
                                                <input type="text" class="form-control" placeholder="N°">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">

                                                <input name="" id="" class="form-control no-label-input" placeholder="Anno">
                                            </div>
                                        </div>
                                          <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">N° inserzione</label>
                                                <select name="" id="" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Prezzo base d'asta</label>
                                                <input type="text" class="form-control" placeholder="Da">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">

                                                <input name="" id="" class="form-control no-label-input" placeholder="A">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-end">
                                            <button type="submit" class="btn btn-success"><i class="fas fa-search"></i> Cerca</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- TAB 2 -->
                            <div class="tab-pane fade" id="type-2" role="tabpanel" aria-labelledby="profile-tab">Mobili</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container py-4 text-center">
        <div class="row">
            <div class="col-md-12">
                <h2>ISTITUTO VENDITE TELEMATICHE - PARTECIPA ALLE MIGLIORI ASTE DA CASA</h2>
            </div>
        </div>
    </div>
    <div class="bg-light">
        <div class="container">
            <!-- Last items -->
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="py-4">
                        <h2>Ultimi Annunci</h2>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-4 offset-md-8">
                    <form action="" class="row">
                        <div class="col-9">
                            <input type="text" class="form-control" placeholder="Filtra per Città/Nazione">
                        </div>
                        <div class="col-3">
                            <button class="btn btn-blue">Applica</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>




            <div id="carouselExampleIndicators" class="carousel carousel-dark slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    @for ($j = 0; $j < 3; $j++)
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="@if($j == 0) active @endif" aria-current="true" aria-label="Slide 1"></button>
                    @endfor
                </div>
                <div class="carousel-inner">
                    @for ($j = 0; $j < 3; $j++)
                    <div class="carousel-item @if($j == 0) active @endif">
                        <div class="container">

                            <div class="row">
                                @for ($i = 0; $i < 8; $i++)
                                    <div class="col-md-3">
                                        <a class="product-thumbnail mb-3 d-block  bg-white" href="#">
                                            <div class="product-thumbnail-head">
                                                <img class="img-fluid" src="https://candbpublichouse.com/wp-content/uploads/2021/05/home-exterior-today-180726-tease_3f99937c609d875fece6a12af1594bd9-12-1536x864.jpg" alt="">
                                                <div class="tag bg-light-blue">IMMOBILE RESIDENZIALE</div>
                                            </div>
                                            <div class="product-thumbnail-body p-3">
                                                <p class="product-thumbnail-address"><i class="fas fa-map-marker-alt"></i> Corso Vittoria Colonna n. 77 Marino (RM)</p>
                                                <p class="product-thumbnail-batch">LOTTO UNICO</p>
                                                <p class="product-thumbnail-dates">
                                                    Data vendita <span class="text-success">24/06/2021 10:30</span><br>
                                                    Puubblicato il <span class="text-success">14/04/2021</span>
                                                </p>
                                            </div>
                                            <div class="product-thumbnail-footer p-3">
                                                <span>Prezzo base d'asta</span><br>
                                                <span class="product-thumbnail-price color-light-blue">
                                                    € 1.982.000,00
                                                </span>
                                            </div>
                                        </a>
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>



        <div class="container">
            <div class="row pb-4 pt-2">
                <div class="col-md-12 text-end">
                    <a href="" class="text-secondary text-decoration-none">Tutti gli ultimi annunci (23011)</a>
                </div>
            </div>
        </div>
        <!-- Expiring items -->
        <div class="bg-blue">
        <div class="container">


                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="py-4">
                            <h2 class="text-white">Ultimi Annunci</h2>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-4 offset-md-8">
                        <form action="" class="row">
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Filtra per Città/Nazione">
                            </div>
                            <div class="col-3">
                                <button class="btn btn-secondary">Applica</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>


            <div id="carouselExampleDark" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    @for ($j = 0; $j < 3; $j++)
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="@if($j == 0) active @endif" aria-current="true" aria-label="Slide 1"></button>
                    @endfor
                </div>
                <div class="carousel-inner">
                    @for ($j = 0; $j < 3; $j++)
                        <div class="carousel-item @if($j == 0) active @endif" data-bs-interval="10000">

                            <div class="container">
                                <div class="row">
                                    @for ($i = 0; $i < 4; $i++)
                                        <div class="col-md-3">
                                            <a class="product-thumbnail mb-3 d-block  bg-white" href="#">
                                                <div class="product-thumbnail-head">
                                                    <img class="img-fluid" src="https://candbpublichouse.com/wp-content/uploads/2021/05/home-exterior-today-180726-tease_3f99937c609d875fece6a12af1594bd9-12-1536x864.jpg" alt="">
                                                    <div class="tag bg-success">IMMOBILE RESIDENZIALE</div>
                                                </div>
                                                <div class="product-thumbnail-body p-3">
                                                    <p class="product-thumbnail-address"><i class="fas fa-map-marker-alt"></i> Corso Vittoria Colonna n. 77 Marino (RM)</p>
                                                    <p class="product-thumbnail-batch">LOTTO UNICO</p>
                                                    <p class="product-thumbnail-dates">
                                                        Data vendita <span class="text-success">24/06/2021 10:30</span><br>
                                                        Puubblicato il <span class="text-success">14/04/2021</span>
                                                    </p>
                                                </div>
                                                <div class="product-thumbnail-footer p-3">
                                                    <span>Prezzo base d'asta</span><br>
                                                    <span class="product-thumbnail-price color-light-blue">
                                                        € 1.982.000,00
                                                    </span>
                                                </div>
                                            </a>
                                        </div>
                                    @endfor
                                </div>
                            </div>

                        </div>
                    @endfor
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
                </div>





            <div class="container">
                <div class="row pb-4 pt-2">
                    <div class="col-md-12 text-end">
                        <a href="" class="text-white text-decoration-none">Tutti gli annunci in scadenza (38727)</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <style>

        .product-thumbnail{
            text-decoration: none;
            color: #212529;
        }

        .product-thumbnail p{
            margin-bottom: 0.7rem;
        }

        .product-thumbnail .product-thumbnail-price{
            font-size: 1.3rem;
        }

        .product-thumbnail .product-thumbnail-head{
            height: 160px;
            overflow: hidden;
            position: relative;
        }

        .product-thumbnail .product-thumbnail-head .tag{
            position: absolute;
            top: 10px;
            left: 10px;
            height: 20px;
            font-size: 0.7rem;
            color: white;
            padding: 2px 4px;
        }

        .product-thumbnail .product-thumbnail-body{
            height: 180px;
        }

        .product-thumbnail .product-thumbnail-footer{
            height: 80px;
        }

        .hero{
            padding: 50px 0px;
            background-repeat: no-repeat;
            background-size: cover;
            background: url('https://geauction.com/wp-content/uploads/2018/07/5-Auction-Tips-for-Beginners2.jpg')
        }
        .hero .hero-content{
            padding: 55px;
        }
        .hero label{
            color: white;
            margin-bottom: .5rem!important;
        }
        .hero a.nav-link{
            color:#fff!important;
            background: #084c79;
            border: none;
            border-radius: 0px;
        }
        .hero a.nav-link.active{
            background: #053559;
        }
        .hero .nav-tabs{
            border: none;
        }
    </style>

@endsection
