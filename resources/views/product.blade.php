@extends('main')
@section('content')

    <div class="container my-4">
        <div class="row mb-4">
            <div class="col-md-12">
                <h2 class="border-bottom">
                    Procedura N.{{ $product->number }}/{{ $product->year }}
                </h2>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        {{ $product->short_description }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <ul class="nav justify-content-center">
                            <li class="nav-item color-">
                                <a class="nav-link active text-center color-blue" aria-current="page" href="#">
                                    <div class="auction-status-icon">
                                        <i class="fa fa-clock fa-2x"></i>
                                    </div><br>
                                    Attesa inizio esame buste
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-center text-body" aria-current="page" href="#">
                                    <div class="auction-status-icon">
                                        <i class="fa fa-envelope fa-2x"></i>
                                    </div><br>
                                    Esame buste in corso
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-center text-body" aria-current="page" href="#">
                                    <div class="auction-status-icon">
                                        <i class="fa fa-hourglass fa-2x"></i>
                                    </div><br>
                                    Attesa avvio gara
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-center text-body" aria-current="page" href="#">
                                    <div class="auction-status-icon">
                                        <i class="fa fa-fire fa-2x"></i>
                                    </div><br>
                                    Gara in corso
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-center text-body" aria-current="page" href="#">
                                    <div class="auction-status-icon">
                                        <i class="fas fa-flag-checkered fa-2x"></i>
                                    </div><br>
                                    Gara terminata
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-6">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                @foreach ($product->photos as $index => $photo)
                                    <button type="button" data-bs-target="#carouselExampleDark"
                                        data-bs-slide-to="{{ $index }}" class="@if ($index==0) active @endif" aria-current="true"
                                        aria-label=""></button>
                                @endforeach
                            </div>
                            <div class="carousel-inner">
                                @foreach ($product->photos as $index => $photo)
                                    <div class="carousel-item text-center @if ($index==0) active @endif" data-bs-interval="10000">
                                        <img height="480" src="{{ $photo }}" class="" alt="...">
                                        {{-- <div class="carousel-caption d-none d-md-block">
                                            <h5>First slide label</h5>
                                            <p>Some representative placeholder content for the first slide.</p>
                                        </div> --}}
                                    </div>
                                @endforeach
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"
                                data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"
                                data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h2 class="text-uppercase text-center color-blue">{{ $product->status }}</h2>
                        <table class="table table-striped mb-4">
                            <tr>
                                <td><b>Prezzo base €</b></td>
                                <td>
                                    <h3>{{ number_format((float) $product->base_price, 2, ',', '.') }}</h3>
                                    <span class="text-muted"><small><i>oltre oneri di legge, come indicato nell'avviso di
                                                vendita.</i></small></span>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Termine per i rilanci</b></td>
                                <td>{!! $product->raise_terms !!}</td>
                            </tr>
                            <tr>
                                <td><b>Inizio vendita</b></td>
                                <td>{!! $product->sale_start_at_html_formatted !!}</td>
                            </tr>
                        </table>
                        <div class="text-center">
                            <a target="_blank" href="{{ $product->auction_subsription_link }}"
                                class="btn btn-blue btn-lg text-white">Iscriviti alla vendita</a><br>
                            <span class="text-muted"><small>Per partecipare a questa vendita è necessario registrare la
                                    propria offerta sul sito del Portale delle Vendite Pubbliche.</small></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h3 class="fw-light text-uppercase">prenota la visita</h3>
                        <p>
                            La prenotazione della visita deve essere effettuata mediante il Portale delle Vendite Pubbliche
                            del Ministero della Giustizia, come stabilito dall'art. 560 c.p.c. Art. 560 c.p.c "... Gli
                            interessati a presentare l'offerta di acquisto hanno diritto di esaminare i beni in vendita
                            entro quindici giorni dalla richiesta. La richiesta è formulata mediante il portale delle
                            vendite pubbliche e non può essere resa nota a persona diversa dal custode. La disamina dei beni
                            si svolge con modalità idonee a garantire la riservatezza dell'identità degli interessati e ad
                            impedire che essi abbiano contatti tra loro."
                            <br>
                            <b>Cliccare su "Prenota visita" per essere reindirizzato al modulo ministeriale:</b>
                        </p>
                        <div class="text-left">
                            <a target="_blank" href="{{ $product->visit_booking_link }}" class="btn btn-blue">Prenota
                                visita</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-4">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h3 class="fw-light text-uppercase">informazioni</h3>
                        <table class="table table-striped">
                            <tr>
                                <td><b>Procedura N.</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Tipo procedura</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Tribunale</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Referente procedura</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Inizio iscrizione</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Termine iscrizione</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Termine visita</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Inizio vendita</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>ID inserzione PVP </b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Codice vendita </b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Data pubblicazione</b></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h3 class="fw-light text-uppercase">dettagli</h3>
                        <table class="table table-striped">
                            <tr>
                                <td><b>Tipo vendita</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Prezzo base</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Rilancio minimo</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Cauzione minima</b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><b>Metodi di pagamento</b></td>
                                <td></td>
                            </tr>
                        </table>
                        <h3 class="fw-light text-uppercase">annotazioni pvpv</h3>
                        <table class="table table-striped">
                            <tr>
                                <td><b>Link inserzione ministeriale </b></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h3 class="fw-light text-uppercase">ubicazione</h3>
                        <b>{{ $product->address['address'] }}</b>
                        <div class="mt-4">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2788.4323929832444!2d9.954471615775173!3d45.66221932822893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478166fe2782a999%3A0xb75a4ab19da43af6!2sVia%20XXIV%20Maggio%2C%2025030%20Paratico%20BS!5e0!3m2!1sen!2sit!4v1627950972502!5m2!1sen!2sit"
                                width="100%" height="320" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#nav-documents"
                                    type="button" role="tab" aria-selected="true">Documenti</button>
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#nav-payment-methods"
                                    type="button" role="tab" aria-selected="false">Modalità di pagamento</button>
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#nav-warranties" type="button"
                                    role="tab" aria-selected="false">Garanzie</button>
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#nav-contacts" type="button"
                                    role="tab" aria-selected="false">Contatti</button>
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#nav-pvp-data" type="button"
                                    role="tab" aria-selected="false">Dati PVP</button>
                            </div>
                        </nav>
                        <div class="tab-content p-4 border" id="nav-tabContent">
                            <div class="tab-pane fade show active " id="nav-documents" role="tabpanel">
                                <table class="table table-striped">
                                    @foreach ($product->documents as $document)
                                        <tr>
                                            <td><b>{{ $document['label'] }}</b></td>
                                            <td><a download
                                                    href="{{ $document['link'] }}">{{ $document['file_name'] }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-payment-methods" role="tabpanel">
                                <b>Modalità versamento cauzione:</b>
                                Come indicato sul Portale delle Vendite Pubbliche
                            </div>
                            <div class="tab-pane fade" id="nav-warranties" role="tabpanel">
                                Si informano gli interessati all'acquisto che le informazioni relative alle descrizioni dei
                                beni sono dettagliatamente indicate nella documentazione allegata.

                                La descrizione e' indicativa delle caratteristiche dei beni da alienarsi, i quali essendo di
                                provenienza giudiziaria (ex art. 2922 c.c. "Nella vendita forzata non ha luogo la garanzia
                                per i vizi della cosa. Essa non puo' essere impugnata per cause di lesione"), sono venduti
                                secondo la formula del "visto e piaciuto", nello stato di fatto e di diritto in cui si
                                trovano, senza alcuna garanzia.
                            </div>
                            <div class="tab-pane fade" id="nav-contacts" role="tabpanel">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th style="width:130px;">Giudice</th>
                                            <td>Angela Randazzo</td>
                                        </tr>
                                        <tr>
                                            <th style="width:130px;">Custode</th>
                                            <td>Michele Vinci</td>
                                        </tr>
                                        <tr>
                                            <th>Telefono</th>
                                            <td>035929216</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><a href="mailto:info@studio-vinci.it" title="Clicca per inviare una email"
                                                    rel="nofollow">info@studio-vinci.it</a></td>
                                        </tr>
                                        <tr>
                                            <th style="width:130px;">Delegato alla vendita</th>
                                            <td>Pietro Bianchi</td>
                                        </tr>
                                        <tr>
                                            <th>Telefono</th>
                                            <td>035231110</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-pvp-data" role="tabpanel">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .auction-status-icon {
            height: 70px;
            width: 70px;
            border-radius: 35px;
            background-color: #ececec;
            display: inline-block;
            padding-top: 20px;
        }

        .carousel-indicators {
            bottom: -56px;
        }

    </style>

@endsection
