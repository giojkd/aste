
    <div class="footer bg-light">
        <div class="container links-container">
            <div class="row">
                <div class="col-md-3">
                    <p class="menu-title">Menu: </p>
                    <ul class="list list-unstyled">
                        @for ($i = 0; $i < 8; $i++)
                            <li>
                                <a href="#">Link {{ $i }}</a>
                            </li>
                        @endfor
                    </ul>
                </div>
                <div class="col-md-3">
                    <p class="menu-title">Menu: </p>
                    <ul class="list list-unstyled">
                        @for ($i = 0; $i < 8; $i++)
                            <li>
                                <a href="#">Link {{ $i }}</a>
                            </li>
                        @endfor
                    </ul>
                </div>
                <div class="col-md-3">
                    <p class="menu-title">Menu: </p>
                    <ul class="list list-unstyled">
                        @for ($i = 0; $i < 8; $i++)
                            <li>
                                <a href="#">Link {{ $i }}</a>
                            </li>
                        @endfor
                    </ul>
                </div>
                <div class="col-md-3">
                    <p class="menu-title">Menu: </p>
                    <ul class="list list-unstyled">
                        @for ($i = 0; $i < 8; $i++)
                            <li>
                                <a href="#">Link {{ $i }}</a>
                            </li>
                        @endfor
                    </ul>
                </div>


            </div>
        </div>
        <div class="bg-blue py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-white text-center">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac vulputate mauris. Sed tincidunt enim et ligula pharetra, vitae rhoncus dolor consectetur.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>
