@extends('main')
@section('content')
<div class="bg-light pt-4">
    <div class="container bg-white">
        <div class="row">
            <div class="col-md-8">
                <h2 class="py-4">
                    Nuova registrazione
                </h2>
                <form action="{{ Route('registerSubmit') }}" method="POST" class="needs-validation" novalidate>
                    @csrf
                    <div class="row border-bottom mb-4">
                        <div class="col-md-3">Informativa</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <p>
                                <small>
                                    Per partecipare alle vendite on line è necessario iscriversi.<br>
                                    In fase di registrazione l'utente può scegliere User e password, quali credenziali per accedere alla propria area riservata.<br>
                                    L'iscrizione è seguita da mail del sistema che si chiede di confermare.<br>
                                    L'utente dovrà inoltre accettare le "condizioni generali di partecipazione alle vendite telematiche" scaricabili dal sito nell'apposita sezione e inviate a mezzo mail all'utente in fase di partecipazione alla gara.<br>
                                    <b>
                                        L'utente, effettuata la registrazione, è responsabile dei propri codici di accesso e pin univoco: dati che non potrà cedere o divulgare a terzi.<br>
                                        Spetta al soggetto registrato aggiornare tempestivamente qualsiasi variazione anagrafica.<br>
                                        L'iscrizione alla piattaforma Fallco Aste di una azienda, va fatta dal rappresentante legale quale persona fisica. Al momento della eventuale presentazione dell'offerta sarà possibile indicare quale soggetto offerente il nome della Azienda.
                                    </b>
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row border-bottom mb-4">
                        <div class="col-md-3">Dati anagrafici</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Nome</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="name" required>
                                    <div class="invalid-feedback">
                                        Il nome è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Cognome</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="surname" required>
                                     <div class="invalid-feedback">
                                        Il cognome è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Codice fiscale</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="fiscal_code" required>
                                     <div class="invalid-feedback">
                                        Il codice fiscale è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-4"></div>
                                <div class="col-8">
                                    <p class="text-muted"><small><i class="fa fa-info-circle"></i> Inserire il codice fiscale estero o altro identificativo se il soggetto risiede all'estero.</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">N° carta identità/ passaporto</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="document_id" required>
                                     <div class="invalid-feedback">
                                        Il numero di un documento è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Data di nascita</label>
                                </div>
                                <div class="col-8">
                                    <input type="date" id="" class="form-control" aria-describedby="" name="date_of_birth" required>
                                    <div class="invalid-feedback">
                                        La data di nascita è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Paese di cittadinanza</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="citizenship_country" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($countries as $country)
                                            <option @if($country->country_name == 'Italy') selected @endif value="">{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        Il paese di cittadinanza è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Nazione nascita</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="country_of_birth" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($countries as $country)
                                            <option @if($country->country_name == 'Italy') selected @endif value="">{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>
                                     <div class="invalid-feedback">
                                        La nazione di nascita è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Regione nascita</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="region_of_birth" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->unique('region')->sortBy('region') as $region)
                                            <option value="">{{ $region->region }}</option>
                                        @endforeach
                                    </select>
                                     <div class="invalid-feedback">
                                        La regione di nascita è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Provincia nascita</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="province_of_birth" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->unique('province')->sortBy('region') as $province)
                                            <option value="">{{ $province->province }}</option>
                                        @endforeach
                                    </select>
                                     <div class="invalid-feedback">
                                        La provincia di nasciat è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Comune nascita</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="comun_of_birth" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->sortBy('name') as $comune)
                                            <option value="">{{ $comune->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        Il comune di nascita è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">CAP nascita</label>
                                </div>
                                <div class="col-8">
                                      <div class="col-8">
                                    <select  id="" class="form-control select2" name="zip_code_of_birth" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->sortBy('postal_code') as $comune)
                                            <option value="">{{ $comune->postal_code }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        Il CAP di nascita è obbligatoria
                                    </div>
                                </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row border-bottom mb-4">
                        <div class="col-md-3">Contatti</div>

                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Email Pec</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="pec" required>
                                    <div class="invalid-feedback">
                                        L'indirizzo Email/PEC è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Telefono</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="phone" required>
                                    <div class="invalid-feedback">
                                        Il telefono è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Cellulare</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="mobile_phone" required>
                                    <div class="invalid-feedback">
                                        Il cellulare è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row border-bottom mb-4">
                        <div class="col-md-3">Residenza</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Nazione</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="residence_country" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($countries as $country)
                                            <option @if($country->country_name == 'Italy') selected @endif value="">{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        La nazione di residenza è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Regione</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="residence_region" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->unique('region')->sortBy('region') as $region)
                                            <option value="">{{ $region->region }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        La regione di residenza è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Provincia</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="residence_province" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->unique('province')->sortBy('region') as $province)
                                            <option value="">{{ $province->province }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        La provincia di residenza è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Comune</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="residence_comun" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->sortBy('name') as $comune)
                                            <option value="">{{ $comune->name }}</option>
                                        @endforeach
                                    </select>
                                     <div class="invalid-feedback">
                                        Il comune di residenza è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Cap/Zip</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="residence_postal_code" required>
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->sortBy('postal_code') as $comune)
                                            <option value="">{{ $comune->postal_code }}</option>
                                        @endforeach
                                    </select>
                                     <div class="invalid-feedback">
                                        La CAP di residenza è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Indirizzo</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="residence_address" required>
                                     <div class="invalid-feedback">
                                        L'indirizzo di residenza è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Civico</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="residence_street_number" required>
                                     <div class="invalid-feedback">
                                        Il numero civido di residenza è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row border-bottom mb-4">
                        <div class="col-md-3">Domicilio</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <label for="" class="col-form-label">Il domicilio è diverso dalla residenza</label>
                                </div>
                                <div class="col-6">
                                    <label for=""><input type="radio" name="domicile_different_from_residence" value="1" id=""> Si</label>
                                    <label for=""><input type="radio" name="domicile_different_from_residence" value="0" id="" checked> No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="domicileDifferentFromResidence" class="d-none">
                             <div class="row border-bottom mb-4">
                        <div class="col-md-3">Domicilio</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Nazione</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="domicile_country">
                                        <option value="">Scegli...</option>
                                        @foreach ($countries as $country)
                                            <option @if($country->country_name == 'Italy') selected @endif value="">{{ $country->country_name }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Regione</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="domicile_region">
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->unique('region')->sortBy('region') as $region)
                                            <option value="">{{ $region->region }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Provincia</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="domicile_province">
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->unique('province')->sortBy('region') as $province)
                                            <option value="">{{ $province->province }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Comune</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="domicile_comun">
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->sortBy('name') as $comune)
                                            <option value="">{{ $comune->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Cap/Zip</label>
                                </div>
                                <div class="col-8">
                                    <select  id="" class="form-control select2" name="domicile_zip_code">
                                        <option value="">Scegli...</option>
                                        @foreach ($comuns->sortBy('postal_code') as $comune)
                                            <option value="">{{ $comune->postal_code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Indirizzo</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="domicile_address">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Civico</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="domicile_street_number">
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row border-bottom mb-4">
                        <div class="col-md-3">Informazioni account</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Username</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="username" required>
                                     <div class="invalid-feedback">
                                        Lo username è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                        <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Password</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="password" required>
                                    <div class="invalid-feedback">
                                        La password è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                        <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Conferma password</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="" class="form-control" aria-describedby="" name="password_confirmation" required>
                                      <div class="invalid-feedback">
                                        La conferma della password è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                        <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Email</label>
                                </div>
                                <div class="col-8">
                                    <input type="email" id="" class="form-control" aria-describedby="" name="email" required>
                                      <div class="invalid-feedback">
                                        Un indirizzo email valido è obbligatorio
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                        <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Conferma email</label>
                                </div>
                                <div class="col-8">
                                    <input type="email" id="" class="form-control" aria-describedby="" name="email_confirmation" required>
                                    <div class="invalid-feedback">
                                        La conferma dell'indirizzo email è obbligatoria
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row border-bottom mb-4">
                        <div class="col-md-12">Condizioni generali di partecipazione alle vendite telematiche</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-8 offset-md-4">
                            <div class="row align-items-center">

                                <div class="col-8">
                                    <div id="terms-and-conditions-container" class="mb-4">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae aliquet elit, non feugiat justo. Sed lectus nisl, pharetra a ultrices id, sodales id neque. Suspendisse mattis ullamcorper tellus, sit amet porttitor dui aliquet eget. Nulla sed quam ante. Nunc viverra sapien eu ipsum laoreet, id efficitur leo imperdiet. Nullam risus risus, sodales eget feugiat nec, placerat sit amet eros. Suspendisse eleifend auctor nisl, sed vulputate ligula faucibus vitae. Sed congue urna sed laoreet malesuada. Nulla facilisi. Vivamus non velit nec odio tincidunt iaculis. Donec ut fermentum ex. Proin consequat leo vel feugiat gravida. Morbi at luctus ligula. Nulla facilisi. Duis a ex in dui pharetra varius id in nibh. In dictum metus quam.Nunc ut efficitur quam. Suspendisse potenti. Proin blandit risus ut ligula vehicula, id vestibulum nisi aliquet. Etiam varius lorem ut nunc auctor aliquam. Mauris finibus eget tortor eget convallis. Nam non sapien accumsan mi cursus interdum. Nunc quis erat justo.
                                    </div>
                                    <label><input type="checkbox"  id="" name="terms_and_conditions" value="1" required> Accetto termini e condizioni</label>
                                    <div class="invalid-feedback">
                                        Devi accettare i termini e le condizioni prima di procedere alla registrazione
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-8 offset-md-4">
                            <button class="btn btn-secondary w-100">Registrati</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<style>
    #terms-and-conditions-container{
        height: 220px;
        overflow: scroll;
    }
</style>

<script>
    $(()=>{
        $('.select2').select2({
            theme:'bootstrap'
        });
        $('input[name="domicile_different_from_residence"]').change(function(){
            if($(this).val() == 1){
                $('#domicileDifferentFromResidence').removeClass('d-none');
            }else{
                $('#domicileDifferentFromResidence').addClass('d-none');
            }
        })

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }else{
                    form.submit();
                }

                form.classList.add('was-validated')
            }, false)
        })

    })


</script>

@endsection


