<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Backpack\PageManager\app\Models\Page;
use App\Models\Country;
use App\Models\Comun;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\Cast\Object_;
use stdClass;

class FrontController extends Controller
{
    //

    public function getDefaultData(){
        return [];
    }

    public function home(Request $request){

        $data = $this->getDefaultData();
        return view('home',$data);

    }

    public function page($slug, $subs = null)
    {
        $page = Page::findBySlug($slug);

        #dd($page);

        $this->data = $this->getDefaultData();

        if (!$page) {
            abort(404, 'Please go back to our <a href="' . url('') . '">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        return view('pages.' . $page->template, $this->data);
    }

    public function register(Request $request){
        $data = $this->getDefaultData();
        $data['countries'] = Country::get();
        $data['comuns'] = Comun::get();
        return view('register', $data);
    }

    public function registerSubmit(Request $request){

        $request->validate([
            "name" => "",
            "surname" => "",
            "fiscal_code" => "",
            "document_id" => "",
            "date_of_birth" => "",
            "citizenship_country" => "",
            "country_of_birth" => "",
            "region_of_birth" => "",
            "province_of_birth" => "",
            "comun_of_birth" => "",
            "zip_code_of_birth" => "",
            "pec" => "",
            "phone" => "",
            "mobile_phone" => "",
            "residence_country" => "",
            "residence_region" => "",
            "residence_province" => "",
            "residence_comun" => "",
            "residence_postal_code" => "",
            "residence_address" => "",
            "residence_street_number" => "",
            "domicile_different_from_residence" => "",
            "domicile_country" => "",
            "domicile_region" => "",
            "domicile_province" => "",
            "domicile_comun" => "",
            "domicile_zip_code" => "",
            "domicile_address" => "",
            "domicile_street_number" => "",
            "username" => "",
            "password" => "",
            "password_confirmation" => "",
            "email" => "",
            "email_confirmation" => "",
        ]);

        $user = User::create([

            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'data' => $request->all(),
            'password' => Hash::make($request->password),

        ]);

        Auth::login($user, true);

        return redirect()->intended(Route('home'));

    }

    public function user(Request $request){
        $data = $this->getDefaultData();
        $data['countries'] = Country::get();
        $data['comuns'] = Comun::get();
        return view('user', $data);
    }

    public function registerSubmitUpdate(Request $request) {

        $request->validate([
            "name" => "",
            "surname" => "",
            "fiscal_code" => "",
            "document_id" => "",
            "date_of_birth" => "",
            "citizenship_country" => "",
            "country_of_birth" => "",
            "region_of_birth" => "",
            "province_of_birth" => "",
            "comun_of_birth" => "",
            "zip_code_of_birth" => "",
            "pec" => "",
            "phone" => "",
            "mobile_phone" => "",
            "residence_country" => "",
            "residence_region" => "",
            "residence_province" => "",
            "residence_comun" => "",
            "residence_postal_code" => "",
            "residence_address" => "",
            "residence_street_number" => "",
            "domicile_different_from_residence" => "",
            "domicile_country" => "",
            "domicile_region" => "",
            "domicile_province" => "",
            "domicile_comun" => "",
            "domicile_zip_code" => "",
            "domicile_address" => "",
            "domicile_street_number" => "",
        ]);

        $data = $this->getDefaultData();

        $user = Auth::user();
        $user->data = $request->all();

        $user->save();

        return back();

    }

    public function logout(Request $request)
    {

        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect(Route('home'));

    }

    public function login(Request $request){

        $data = $this->getDefaultData();
        return view('login', $data);

    }

    public function loginSubmit(Request $request){

        #dd($request->all());

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        #dd($credentials);

        if (Auth::attempt($credentials, true)) { #true remembers login
            $request->session()->regenerate();
            return redirect()->intended('home');
        }

        return back()->withErrors([
            'email' => 'Ouch! Le credenziali inserite non corrispondono a quelle di nessun utente.',
        ]);

    }

    public function product(Request $request, $name, $id){

        $data = $this->getDefaultData();
        #$product = Product::find($id);

        $product = new Product();
        $product->number = '184';
        $product->year = '2019';
        $product->visits = 20;
        $product->short_description = 'Codice lotto: 12 Quota 1/1 di piena proprietà delle unità immobiliari in Comune di Paratico (BS) censite al NCEU, Sezione Urbana NCT, foglio 5, Via XXIV Maggio n. snc: - mappale 420/27, piano S1, categoria C/2, classe 2, consistenza mq. 5, RC Euro 9,81;';
        $product->status = 'gara non avviata';
        $product->won = 0;
        $product->base_price = 11250;
        $product->total_base_price = 12487.25;
        $product->bids_count = 4;

        $product->closes_at = '2021-07-21 12:00:00';
        $product->attempts_count = 2;
        $product->type = 'ESECUZIONI MOBILIARI CON VENDITA POST LEGGE 80';
        $product->court = 'Bergamo';
        $product->referent = ['name' => 'Parva Domus Srl', 'phone' =>  '(0354284671)', 'email' => 'info@parva-domus.it'];
        $product->subscription_begins_at = '2021-08-02 15:37:00';
        $product->subscription_terms = [
            [
                'type' => 'bonifico',
                'date' => '2021-09-16 12:00:00'
            ],
            [
                'type' => 'assegno',
                'date' => '2021-09-16 12:00:00'
            ]
        ];
        $product->visits_end_at = '2021-09-16 12:00';
        $product->sale_start_at = '2021-09-17 09:30';
        $product->pvp_listing_id = '1389402';
        $product->pvp_listing_link = 'https://pvp.giustizia.it/pvp/it/dettaglio_annuncio.page?contentId=LTT5629557&idInserzione=1389402';
        $product->auction_subsription_link = 'https://pvp.giustizia.it/pvp-offerta/i/1389402?lang=it';
        $product->id = '495017';
        $product->published_at = '2021-08-02 15:53';
        $product->visit_booking_link = 'https://pvp.giustizia.it/pvp/it/prenotazione_visita.page?contentId=LTT5629557&beneId=1805816';
        $product->documents = [
            [
                'label' => 'Avviso di vendita	',
                'file_name' => 'Avviso di vendita_Fall. Costruzioni Edili Ghirardelli_asta del 17.09.2021.pdf',
                'link' => 'https://d3h8bn4njg0vla.cloudfront.net/00/32/00/66/0032006605.pdf'
            ],
            [
                'label' => 'Perizia beni',
                'file_name' => 'USARE_SN_ESTRATTO PERIZIA IMMOBILI_Fall. Costruzioni Edili Ghirardelli.pdf',
                'link' => 'https://d3h8bn4njg0vla.cloudfront.net/00/32/00/66/0032006606.pdf'
            ],
        ];
        $product->payment_method = 'Il bonifico per la cauzione degli offerenti telematici dovrà effettuarsi sul conto corrente i cui estremi sono: IBAN: IT 65 Y 03267 11100 000020390918 Beneficiario: Fall. Costruzioni Edili Ghirardelli';
        $product->sale_type = '';
        $product->minimum_raise = 100;
        $product->raise_terms = '<h3 class="d-inline">60 sec.</h3> <span class="text-muted"><small><i>( 1 min. )</i></small></span> ';
        $product->minimum_bail = '10,00% prezzo offerto';
        $product->address = [
            'address' => 'Via XXIV Maggio - Paratico (BS)'

        ];
        $product->warranties = 'Si informano gli interessati all\'acquisto che le informazioni relative alle descrizioni dei beni sono dettagliatamente indicate nella documentazione allegata. La descrizione è indicativa delle caratteristiche dei beni da alienarsi, i quali essendo di provenienza giudiziaria (ex art. 2922 c.c. "Nella vendita forzata non ha luogo la garanzia per i vizi della cosa. Essa non può essere impugnata per cause di lesione"), sono venduti secondo la formula del "visto e piaciuto", nello stato di fatto e di diritto in cui si trovano, senza alcuna garanzia.';
        $product->photos = [
            'https://d3h8bn4njg0vla.cloudfront.net/00/32/00/66/0032006614.jpg',
            'https://d3h8bn4njg0vla.cloudfront.net/00/32/00/66/0032006608.jpg',
            'https://d3h8bn4njg0vla.cloudfront.net/00/32/00/66/0032006610.jpg',

        ];

        $data['product'] = $product;

        return view('product', $data);

    }

}


